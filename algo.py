import networkx as nx
from sortedcontainers import SortedSet


def read_graph(name_file):
    """
    Format file
    m - count of edge
    s - start vertex
    m lines with edge (s,to, weight)

    :param name_file:
    :return:
    """
    f = open(name_file, "r")
    m = int(f.readline())
    result = nx.Graph()
    start = int(f.readline())
    for i in range(m):
        s, t, c = f.readline().split(" ")
        result.add_edge(int(s), int(t), weight=int(c))
        result.add_edge(int(t), int(s), weight=int(c))
    return result, start


def get_path(p, t):
    result = []
    while p[t] != -1:
        result.append(t)
        t = p[t]
    result.append(t)
    return list(reversed(result))


def dijkstra(s, graph):
    d = []
    order_set = SortedSet()
    p = []
    for i in range(len(graph.node)):
        if i != s:
            order_set.add((100000, i))
        else:
            order_set.add((0, i))
        d.append(100000)
        p.append(-1)

    d[s] = 0

    while len(order_set) > 0:
        dist, first = order_set.pop(0)
        for _, t in graph.edges(first):
            c = graph.get_edge_data(first, t)["weight"]
            if dist + c < d[t]:
                order_set.remove((d[t], t))
                d[t] = dist + c
                order_set.add((dist + c, t))
                p[t] = first

    return d, p


def calculate(name_file):
    graph, start = read_graph(name_file)
    d, p = dijkstra(start, graph)
    print(d)
    for i, v in enumerate(d):
        if i != start:
            print("path from  " + str(start) + " to " + str(i) + " " + str(get_path(p, i)))


calculate("test.in")
